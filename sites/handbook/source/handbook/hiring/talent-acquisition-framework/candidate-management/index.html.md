---
layout: handbook-page-toc
title: "Candidate Management Processes"
description: "Provide guidance on managing candidates in Greenhouse."
---

{::options parse_block_html="true" /}

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}


## Candidate Management Processes
**Purpose**: To provide guidance on managing candidates in Greenhouse.

### Greenhouse Sourcing

**How to Transfer a Candidate That Was Rejected to a Different Requisition**

If you are sourcing through Greenhouse and want to consider a rejected candidate for a different requisition:
1. In the candidate's Greenhouse profile, navigate to the right sidebar and select `Add, Transfer, or Remove Candidate's Jobs`.
1. Select the option to `Add to Another Job`.
1. Select the appropriate "Job" and "Stage" you want to transfer the candidate to > `Add to Job`.
1. The candidate's profile should now display multiple jobs. Select the job you transferred them to.
1. Under the job's title, select the `Pencil Icon` to change the source to `Greenhouse Sourcing` and include the name of the person responsible for sourcing.

**How to Transfer a Candidate to a Different Requisition**

If you want to consider a candidate for a different requisition:
1. In the candidate's Greenhouse profile, navigate to the right sidebar and select `Add, Transfer, or Remove Candidate's Jobs`.
1. Select the option to `Add to Another Job`.
1. Select the appropriate "Job" and "Stage" you want to transfer the candidate to > `Add to Job`.
1. The candidate's profile should now display multiple jobs. Select the job you transferred them to.
1. Under the job's title, be sure to select the `Pencil Icon` to update the source if necessary.

**How to Transfer a Rejected Candidate to the Same Requisition**

If you want to consider a candidate for the same requisition they were rejected from:
1. In the candidate's Greenhouse profile, navigate to the right sidebar and select `Add, Transfer, or Remove Candidate's Jobs`.
1. Select the option to `Add to Another Job`.
1. Select the appropriate "Job" and "Stage" you want to transfer the candidate to > `Add to Job`.
1. The candidate's profile should now display multiple jobs. Select the job you transferred them to.
1. Under the job's title, be sure to select the `Pencil Icon` to update the source if necessary.

### Reviewing Candidate Applications

**How to Access the Review Applications Feature in Greenhouse**

While only the assigned *Recruiter* has the `Applications to Review` widget on their Greenhouse dashboard, other `Job Admin` users can access that feature one of two ways. Those being:

1. By accessing a requisition's `Job Dashboard` tab and referencing the `Pipeline` box in the upper right-hand corner.
    * If there are candidates in the `Get to Know Us` stage, a green box will appear that reads, `Review Applications`.
    * Once inside that feature, you'll be presented with a overall view of the candidate's details and resume, if provided.
    * From this page, a candidate can either be `Advanced`, `Skipped`, or `Rejected` (upper right-hand corner).
    * Additionally, feedback can be left by clicking the `Leave Feedback` button.
1. By searching via the `All Jobs` tab and filtering to see how many new candidates there are to review.
    * To view only your requisitions, please set the following filters:
        * **Job Status** = `Open` AND...
        * **User** = `Me` AND...
        * **Role** = `Sourcer` 
    * The *Sourcer* can then see how many candidates are in the requisition, including how many are "New." To review applications, the user would then click into a requisition and click `Review Applications` to review the applications.
    * Alternatively, a *Sourcer* could click into the `Candidates` tab of a requisition, make sure that the following filters are set, and review applications that way.
        * **Status** = `Active` (Profile Details > Status)
        * **Job Status** = `Open` (Jobs > Filter by Job)
        * That the `Requisition Title` is selected
            * This will be auto-selected when accessed via a specific requisition
        * **Stage** is sorted `(early to late)`
