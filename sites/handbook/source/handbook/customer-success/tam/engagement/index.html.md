---
layout: handbook-page-toc
title: "Account Engagement"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

View the [TAM Handbook homepage](/handbook/customer-success/tam/) for additional TAM-related handbook pages.

---

We are currently developing new account segmentation which will be implemented in the future. Please see the [Customer Segments](/handbook/customer-success/tam/customer-segments-and-metrics/) page for details.
{: .alert .alert-info}

There are three models currently offered for Technical Account Manager engagement. These are broken into tiers that currently use Annual Recurring Revenue as a metric for determining a manageable volume for a single Technical Account Manager and the depth of involvement during the engagement.

## Managing the Customer Engagement

Technical Account Managers will typically manage customer engagements via a GitLab project in the [`account-management` group](https://gitlab.com/gitlab-com/account-management/). This project will be based off the [Enterprise](https://gitlab.com/gitlab-com/account-management/customer-collaboration-project-template) or the [Commercial](https://gitlab.com/gitlab-com/account-management/commercial/templates/new-customer-project/) Customer Success Plan Template and customized to match the customer's needs as outlined above. The project is pre-loaded with milestones, issues, labels, and a README template to help kick off the project and outline a proof of concept, implementation, and customer onboarding. The following is a short [introduction video](https://youtu.be/b8D67EJjL9w) on GitLab's first iteration of the Customer Success Plan.

### Start a new customer engagement

1. Somewhere between step 3 and step 7 of the customer journey sequence, a Solutions Architect should create a project for the customer in GitLab and include a Professional Services Engineer and Technical Account Manager who are best aligned with the customer account. This typically only applies to Enterprise accounts.
2. After the Technical Account Manager has been aligned with the account, they will assign themselves to the “Technical Account Manager” field within Salesforce.
3. The Technical Account Manager confirms that a new customer project has been created based on the [Enterprise](https://gitlab.com/gitlab-com/account-management/customer-collaboration-project-template) or the [Commercial](https://gitlab.com/gitlab-com/account-management/commercial/templates/new-customer-project/) Customer Success Plan Template. If it hasn't, they need to create it and work with the Strategic Account Leader/Account Executive and/or Solutions Architect to complete it. This _should_ have been done prior to Technical Account Management involvement for all Enterprise accounts. For Commercial accounts, the TAM will create the project once they receive the account and determine it is beneficial to have a customer project.
4. Follow the steps in the PLEASE-READ-THESE-INSTRUCTIONS.md file.

### Start a customer upgrade engagement

1. After the Technical Account Manager has been aligned with the account, they will assign themselves to the “Technical Account Manager” field within Salesforce.
1. Provided that the customer is part of the [TAM-Assigned segment](https://about.gitlab.com/handbook/customer-success/tam/customer-segments-and-metrics/#tam-assigned-segment), confirm that the customer project has been created previously during the customer journey sequence, and if not available create a project for the customer in GitLab and include a Technical Account Manager who is best aligned with the customer account transition. For Commercial accounts, the TAM will determine if it is beneficial to have a customer project.
1. Verify that the project complies with the [Enterprise](https://gitlab.com/gitlab-com/account-management/customer-collaboration-project-template) or the [Commercial](https://gitlab.com/gitlab-com/account-management/commercial/templates/new-customer-project/) Customer Success Plan Template.
1. Follow the steps in the PLEASE-READ-THESE-INSTRUCTIONS.md file.

### Where does a Technical Account Manager fit in?

During the pre-sales process for Enterprise accounts, a Solutions Architect owns the project with assistance from the Strategic Account Leader and should include the Professional Services Engineer if there is one assigned. A Technical Account Manager is involved but only for visibility. Until the account becomes a paying customer the project remains in pre-sales. Once the customer has paid, the Strategic Account Leader will set up the "Welcome to GitLab" call along with the key GitLab employees (SAL, SA, PSE and Technical Account Manager) and the customer. There is a preloaded issue for this in the project template.

For Commercial accounts, the Account Executive owns the pre-sales process and [engages a Solutions Architect](/handbook/customer-success/solutions-architects/#commercial-engagement-model) as needed. Once the account becomes a paying customer, the Technical Account Manager will create a customer project if it will be useful to their relationship with the customer, and the Account Executive will schedule a "Welcome to GitLab" call with the customer and the Technical Account Manager.

The "Welcome to GitLab" call will introduce the customer to the Technical Account Manager and begin the handover process. The Technical Account Manager will then lead the rest of the call and own the customer project. If the project was created in the pre-sales project under the [`pre-sales account-management` group](https://gitlab.com/gitlab-com/account-management/pre-sales), then it is moved to a post-sales project under [`account-management` group](https://gitlab.com/gitlab-com/account-management).

### Customer Engagement Tips
1. Make use of the [account management projects](https://gitlab.com/gitlab-com/account-management)  ([Template](https://gitlab.com/gitlab-com/account-management/templates/customer-collaboration-project-template))
1. Whenever a customer asks a question via email/Slack, kindly prompt them to open a Support ticket or an issue in the collaboration project
  * Examples of a Support issue: Reporting GitLab downtime, errors or accessibility problems
  * Examples of a Collaboration Project Issue: General questions about product usage, best practices or recommendations, adoption or integration questions
1. Show customers the power of GitLab by using GitLab with them (ex: related issues, tracking milestones, threaded conversations, ability to interact directly with GitLab’s product/engineering teams)
1. Make sure you are responding in a timely manner to customer's open issues. When you are prompt to respond, the customer is more likely to continue using the account management projects
1. Have discussions in the public issues as much as possible. If there is a question/discussion point that requires interaction with the GitLab Engineering and Product teams, post it in the public issue, as it most likely benefits everyone reading the public issue.
1. When adding a SFDC link to a public issue, remember to [provide feedback](/handbook/product/how-to-engage/#a-customer-expressed-interest-in-a-feature) to the product managers using the [feedback template](/handbook/product/how-to-engage/#feedback-template)
1. In-person meetings are some of the most impactful and productive engagements, but they are also the most expensive in terms of time and money. To make sure both parties get the most out of an in-person meeting have an agenda planned well in advance and send the agenda to all parties attending the meeting. Also, if a GitLab E-group member is attending the meeting, an [Executive Briefing Doc](https://docs.google.com/document/d/1hyA12EN5iEwApAr_g4_-vhUQZohKxm5xkX9xxZ1JNog/edit) (internal link) will also be required (see the next section for more details on engaging with an e-group member).

### E-Group Customer Calls

Occassionally, a GitLab e-group member (VP or C-level) will be meeting with a customer, for example, as part of an executive briefing, escalation, etc. Please review the [EBA handbook page](/handbook/eba/) on guidance on how to schedule with the E-Group, specifically the section on [customer, prospect, and partner meetings with an E-Group member](/handbook/eba/#customer-prospect-and-partner-meetings-with-an-e-group-member). There will be a prep call prior to the customer call (typically the day before), and there is an [Executive Briefing Document](https://docs.google.com/document/d/1hyA12EN5iEwApAr_g4_-vhUQZohKxm5xkX9xxZ1JNog/edit) (internal doc link) that must be prepared and shared with the EBA in advance of the prep call, in addition to any materials that will be used during the customer call. Below are some tips to ensure a positive experience:

1. Work on the prep doc and call resources with your account team.
1. Be as thorough as possible about your customer's details (e.g. ARR, # of users, tier, renewal date, stages in use)
1. Ensure you answer the 5 W's (who,what,where,when,why) of the meeting and what should be the outcome of the meeting.
    - Make sure that we detail both what GitLab wants out of the meeting and what the customer wants out of the meeting. This is a collaborative partnership that benefits both parties.
1. Be prepared in advance of what you want the E-Group member to do, say, and ask.
1. Have an agenda for the call and ensure everyone knows what they're supposed to do (who will speak when and about what).
1. Add any topics you think the customer may bring up (positive or negative) and advice on how to approach it.
1. Provide links wherever possible (e.g. documentation, feature requests, blockers, etc.).
1. To add a deep-link to the Gainsight Success Plan, go to the TAM Portfolio dashboard, scroll down to the "Success Plans by Account" widget, expand the report (click the square on the top bar of the report), then click the link under the "Success Plan" (not the link in the account name column to the C360); copy and paste this link to the document. It's typically recommended to use the Gainsight direct login, rather than the Salesforce login, for this purpose.

For an example of a prep doc and additional materials that received positive feedback, please visit this **internal only** [Google doc](https://docs.google.com/document/d/1Ym7DUw4jfol3QHy4TaiDXlB__u5-oev85WiYrs5apGg/edit).

### Disengaging with a customer

There are situations when a TAM needs to disengage with a customer. Examples include:

- A customer downgrades or churns and is [below the TAM alignment threshold](/handbook/customer-success/tam/services/#tam-alignment)
- A TAM has been engaged with a customer [below the TAM alignment threshold](/handbook/customer-success/tam/services/#tam-alignment) for a strategic purpose such as a tier upgrade that was unsuccessful

When this happens, it is important to manage the disengagement so that the customer understands the reason, and is clear on who they should communicate with going forward. Here are some recommendations for how to have this conversation:

- Do it in person (or face-to-face via Zoom), and include the SAL/AE since they will retain the relationship.
- Be honest about the reason. Each situation will be different, and it's a good idea to talk it over with your manager.
- Go over their options for assistance going forward (below).
- Send a follow-up that summarizes what was discussed, including their assistance options.

Recommended options to review with the customer include:

- Troubleshooting, errors, outages, and basic "how to" technical questions should go to Support.
- Complex "how to" and workflow advice should be sent _in writing_ to the SAL/AE. Doing it in writing lets them work with the whole of Customer Success asynchronously to get the best solution.
- If disengagement is due to the customer downgrading/churning, identify the features they will lose access to with the [feature comparison](https://about.gitlab.com/pricing/self-managed/feature-comparison/) page. 
- Online resources such as the GitLab documentation, blog, YouTube channels, [community forum](https://forum.gitlab.com/) and other relevant materials that they can for self-serve guidance.

## Customer Personas

Technical Account Managers focus on helping customers achieve business outcomes by advising and enabling customers on usage of GitLab features. We are able to do this most effectively when we are engaged with customer personas who are responsible for the capabilities that GitLab provides.

We have defined two key personas that TAMs should regularly engage with and align on outcomes and use cases:

### Development Lead

A member of the customer's development or engineering department, in a leadership role. This person is aware of and/or responsible for delivering on customer business objectives related to the customer's software production. They are able to speak to development workflows, and SDLC and DevOps practices and challenges.

### Software Security Lead

Has responsibility for the security of the software the customer develops. This person is able to speak to business requirements, objectives, compliance frameworks, etc. that the customer has related to the software they produce. They have knowledge of (and possibly own) the customer's current security scanning & management tools.

### Persona Enablement

We have [role-play scenarios to practice identifying and gaining access to the defined personas](/handbook/customer-success/tam/roleplays/#customer-personas).

## Customer Prioritization

GitLab uses a customer prioritization model and [associated metrics](https://about.gitlab.com/handbook/customer-success/tam/customer-segments-and-metrics/) based on their customer segment (Strategic, Mid-Touch, Scale) to focus the TAM's efforts where they will have maximum value and impact. 

On the Gainsight Attributes section, the TAM can set the priority level per customer with levels 1 or 2, with 1 being the highest. A new TAM-assigned customer coming on to GitLab will default to Pr1 until their onboarding enablement is complete. Pr3 is solely for unmanaged child accounts, and Pr4 is only for our digital customers and not for the TAM-assigned segment, with the exception of the Public Sector that is trialing a TAM-assigned digital customer. Priority definitions vary by TAM segment.

![Post-Sales Customer Segmentation](https://lucid.app/publicSegments/view/8a23ea4e-843c-47be-9337-d819c158efac/image.jpeg "Post-Sales Customer Segmentation")
<br>

Why do we use a prioritization system?

- To enable focus on customers that have an imminent opportunity for growth or that require near-term risk mitigation
- To ensure that their book of business does not overburden the TAMs
- To give TAM Managers more visibility into the potential workload of their team via more context on the makeup of the overall portfolio
- Used to further segment customers, beyond the [Sales Segmentation](https://about.gitlab.com/handbook/sales/field-operations/gtm-resources/#segmentation)

The `TAM Portfolio` Dashboard is used to help highlight and review each client, including their priority level.

### Strategic Accounts

Priority definitions are as follows:
1. Pr1: TAM-led onboarding, monthly cadence call (as minimum), full TAM-led customer lifecycle
1. Pr2: 60 day call cadence, EBR, renewal touch point
1. Pr3: Non-managed child accounts only

TAM-assigned customers are segmented into two priority tiers: `Priority 1 (P1)` and `Priority 2 (P2)`. We use a series of "yes/no" parameters to evaluate a customer's prioritization, based on the key aspects of a TAM's responsibilities and value to the customer. There may be rare use of a `Priority 3 (P3)` tier, wherein an account is a child account that is not actively managed outside of its parent account but is still within the Strategic segment.

Each TAM should aim to have roughly one third (1/3) of their book of business listed as `Priority 1`, and the rest as `Priority 2`.

#### Determining priority

##### Parameters

The following parameters are considered when determining customer prioritization:

| Parameter                                                                     | Yes                                                                                                 | No                                                                                                               |
|-------------------------------------------------------------------------------|-----------------------------------------------------------------------------------------------------|------------------------------------------------------------------------------------------------------------------|
| [**LAM**](/handbook/sales/sales-term-glossary/#landed-addressable-market-lam) | High degree of confidence, or explicit evidence, of greater than 10%                                | Uncertain about LAM, or know for sure it is less than 10%                                                        |
| **Growth in the next 6 months**                                               | Open growth opportunity in Salesforce, and/or explicitly stated intent from the customer for growth | Uncertain about growth in the given timeframe, or the customer has clearly said they have no growth in that time |
| **Open tier upgrade**                                                         | Upgrade opportunity is open in Salesforce and actively being discussed with the customer            | No open opportunity in Salesforce, and/or no active upgrade discussion with the customer                         |
| **Current/upcoming stage adoption**                                           | Actively working with the customer on planning or implementation                                    | Nothing actively being worked on, or only discussed as a future initiative                                       |
| **Current/upcoming infrastructure change**                                    | Actively working with the customer on planning or implementation                                    | Nothing actively being worked on, or only discussed as a future initiative                                       |
| **Contraction or Churn risk**                                    | Account in Triage process                                    | No contraction or churn risk identified                                      |
| **Customer lifecycle stage**                                    | Customer in onboarding                                    | Onboarding completed successfully                                      |

##### Prioritization model

Using the defined parameters and guidance on how to know whether they are applicable, we can use the following flowchart to evaluate priority:

<!-->
Online Mermaid editor: https://mermaid-js.github.io/mermaid-live-editor
<-->
```mermaid
graph TD;
  upgrade[Open up-tier opp closing in next 6 months]-- Yes -->p1;
  upgrade-- No -->stage;
  stage[Stage adoption this quarter requiring >2hrs TAM time per week]-- Yes -->p1;
  stage-- No --> infra;
  infra[Infra change this quarter requiring >2hrs TAM time per week]-- Yes -->p1;
  infra-- No -->churnrisk;
  churnrisk[At risk of contraction or churn]-- Yes -->p1;
  churnrisk-- No -->onboarding;
  onboarding[First 30-45 days of onboarding]-- Yes -->p1;
  onboarding-- No -->p1exception;
  p1exception["P1 exception*"]-- Yes -->p1;
  p1exception-- No -->p2;
  p1((Priority 1));
  p1-.->p2exception;
  p2exception["P2 exception*"]-- Yes -->p2;
  p2{{Priority 2}};
```

_* Please see [priority exceptions](#priority-exceptions) for details._

##### New customer onboarding

When a [new customer is in onboarding](/handbook/customer-success/tam/onboarding/), they are automatically `Priority 1`. This is a key lifecycle event for a customer, which warrants high engagement from the TAM. Once onboarding is complete, the customer's prioritization should be reassessed.

#### Keeping prioritization updated

Since the parameters used to evaluate customer prioritization will change over time, each account's priority should be reviewed and updated **quarterly**, coinciding with [QBRs](/handbook/sales/qbrs/).

#### Priority exceptions

There are occasions that warrant a customer to be prioritized differently than the model indicates. In cases where the TAM and/or SAL believes this is true, an exception may be made.

Examples of when an exception may be appropriate include:

- A large, strategic customer with no [LAM](/handbook/sales/sales-term-glossary/#landed-addressable-market-lam)
- A customer with large [LAM](/handbook/sales/sales-term-glossary/#landed-addressable-market-lam) but no clear & active growth path
- A customer with [risk](/handbook/customer-success/tam/health-score-triage/) (which is not factored into the prioritization model) which requires higher engagement to address and mitigate the risk)

In order to make an exception, the TAM or SAL should discuss the details with the members of the [account team](/handbook/customer-success/account-team/#enterprise) and their respective managers. Exceptions will be addressed to the regional Sales Management team and documented in Gainsight.

### Mid-Touch Accounts

The Mid-Touch segment aims to have no more than 20% of a TAM's book of business to be Priority 1 at any given time.

For a customer to be a Priority 1, they must meet at least one of the following parameters:
- User growth and/or uptier within the next 6 months (must have a corresponding Salesforce opportunity)
- Active stage adoption planning and/or implementation
- Contraction or churn risk
- Onboarding

The primary difference between Priority 1 and Priority 2 customers is the frequency of synchronous engagement:
- **Priority 1:**
   - Monthly cadence calls
   - For onboarding: weekly calls for first 30 days
- **Priority 2:**
   - Cadence calls every 60-90 days

Priority 1 customers will also have more focus on having EBRs to ensure alignment on use case adoption and future expansion.

### Scale Accounts
The Scale Account prioritization model will be defined in FY23Q1.

### Commercial (NORAM, LATAM, EMEA)

#### Determining priority
All new Commercial customers default to `Priority 1` to start their engagement. This ensures sufficient time for discovering customer needs and orienting them to working with GitLab (Support plan, success planning, available services and training, etc).

While engaging with a customer, the TAM then determines the appropriate engagement model below needed to ensure long-term success with GitLab. Based on the criteria below, the TAM essentially qualifies the account for a lower level of engagement, moving from highest to lowest engagement.

#### Qualification Criteria

**<u>Priority 1</u>**
- **ARR:** Equal or Greater than $200k 
     OR 
- **In-Onboarding:** Customer is in their first 45 days with GitLab
- **Success Planning:** Ongoing critical initiative that requires immediate TAM enablement/ workshops
- **Growth in the next 3 months:**  Open growth opportunity in Salesforce, and/or explicitly stated intent from the customer for growth
- **Stages adopted:**  Not already using 2-3 stages
- Account at-risk or in triage ([handbook](https://about.gitlab.com/handbook/customer-success/tam/health-score-triage/#gitlab-account-triage-project))

**<u>Priority 2</u>**
* All remaining non-PR 1 customers

For each priority level, we follow the ARR rule OR some combination of the other rules.  So an account could have clear objectives and be using 5 stages, but if are $200k ARR or above they remain priority 1. For smaller accounts with low upside, or low stage adoption, we will mark them `Priority 1` only until we have captured Objectives, so that Sales can have more specific conversations when a TAM isn't fully engaged.

In the future we may consider the following:
- Metrics:  SaaS or usage ping enabled
- Professional Services or Channel partner engaged

A TAM may choose to qualify an account up to a higher level of engagement based on the following considerations:
- Uptier opportunity identified w/ timeline (prefer 9 months or less)
- At-risk triage meeting determines the customer needs more attention (including negative NPS)
- Key logo or case study agreement
- Possible channel engagement or initiatives affecting multiple end customers

#### Engagement Guidelines

For each level of engagement above, the TAM is expected to provide the following services:

**<u>Priority 1</u>**
- **Onboarding ([handbook](https://about.gitlab.com/handbook/customer-success/tam/onboarding/#time-to-onboard)):**  Kick-off call + meet every 2-4 weeks
- **Frequency:**  Monthly status/consulting calls
- **Success Planning:**  Fully “green” success plan ([handbook](https://about.gitlab.com/handbook/customer-success/tam/success-plans/#create-a-success-plan-in-gainsight)) with at least 3 objectives
- **Executive Business Reviews** ([handbook](https://about.gitlab.com/handbook/customer-success/tam/ebr/)):  1-2 per year
- **Q&A:**  Live calls and email

**<u>Priority 2</u>**
- **Onboarding:**  Kick-off call + digital, check-in calls every 2-4 weeks
- **Frequency:**  Quarterly or semi-annual meetings
- **Success Planning:**  Objectives only, no tasks. Strategy and Highlights optional (refer to Command Plan)
- **Executive Business Reviews:**  TAM discretion
- **Q&A:**  Inbound TAM email or Support ticket

