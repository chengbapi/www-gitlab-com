#!/usr/bin/env ruby
#
# This script gets the YouTube thumbnail for referenced YouTube videos and downloads the image file so that it can be referenced localy for better performance. This should be run every once in a while to get the latest thumbnails.
#
# Expects yml files that are lists of assets where each asset has at least the following defined:
#
#  - asset_id
#  - thumbnail_url
#  - thumbnail_file
#

require 'yaml'
require 'uri'
require 'open-uri'
require 'fileutils'

# WARNING. These directories are not absolute so must be adjusted based on where you are running this file from. It is currently expecting to be run from /helpers.
YML_DIR = "../data/learn/paths".freeze
SRC_DIR = "../source".freeze

def main
  # See if we should not create anything for the given run
  # @debug_mode = false
  @debug_mode = true if (ARGV.include? '-d') || (ARGV.include? '--debug')

  Dir["#{YML_DIR}/*.yml"].each do |f|
    puts "Processing file: #{f}"

    # For every YoutTube asset in every file, download the thumbnail image
    # (skips if thumbnail_url isn't a key, assuming asset isn't YouTube video)
    YAML.safe_load(File.read(f)).each do |asset|
      if !asset['thumbnail_url'].nil? && !asset['thumbnail_url'].empty? && !asset['thumbnail_file'].nil? && !asset['thumbnail_file'].empty?
        puts "  Processing thumbnail: #{asset['thumbnail_url']} for asset #{asset['asset_id']}"
        download_image asset['thumbnail_url'], "#{SRC_DIR}#{asset['thumbnail_file']}"
      end
    end
    puts " "
  end
end

def download_image(image_url, full_filename)
  destination_dir = File.dirname(full_filename)

  # create directories in path if they don't already exist
  FileUtils.mkdir_p destination_dir

  # Download the image and write it to a file in the destination_dir
  if @debug_mode.nil?
    puts "    Getting and writing: #{full_filename}"
    image_contents = URI.open(image_url).read
    File.open(full_filename, 'wb') { |file| file.write image_contents }
  else
    puts "    Debug mode on . . . NOT writing: #{full_filename}"
  end
end

main
