---
layout: handbook-page-toc
title: "Content library - life at GitLab"
---

# Welcome to the life at GitLab content library

This content library is a curated list of blog posts, articles, videos, awards, and quick facts that help tell the story of life at GitLab.

# How to use the content library

Whether you're on the Talent Acquisition team or another team at GitLab, we want all team members to have the information they need to spread the word about GitLab and what it's like to work here.

Use this content library as a resource whenever you're acting as a talent ambassador for GitLab: When you're sharing on social media, having conversations with people in your network, speaking at an event, etc.

# Life at GitLab at a glance

## Quick facts

| Fact | To date 2022-01 |
| ------ | ------ |
| Headcount | 1,600+ |
| Countries | 67 |
| Remote team members | 100% |
| Pets | 330+ |

Source: [GitLab team page](/company/team/)

## Key messages about life at GitLab

- We're one of the largest all-remote companies in the world, and we've intentionally built our culture this way from our founding year.
- GitLab is a place where you can contribute from almost anywhere around the globe.
- We have an incredibly unique culture and working environment that's unlike any company you've experienced. This is an ambitious, productive team that embraces a [set of shared ​values and behaviors](/handbook/values/)​ in everything we do.
- More about [life at GitLab and our culture](/company/culture/).


## Key messages about diversity, inclusion, and belonging (DIB) at GitLab

- Diversity, inclusion, and belonging (DIB) are fundamental to the success of GitLab. We infuse DIB in every way possible in all that we do.
- We strive for a transparent environment where all globally dispersed voices are heard and welcomed. We strive for an environment where people can show up as their full selves each day and can contribute to their best ability. And with over 100,000 organizations utilizing GitLab across the globe, we strive for a team that is representative of our users.
- At GitLab, we have team member resource groups (TMRGs), inclusive benefits, training and development, an inclusive hiring process, outreach initiatives to underrepresented groups, a Global DIB Advisory Group, and more.
- More about [DIB at GitLab](/company/culture/inclusion/).

# Awards and lists recognizing GitLab as a great place to work

These awards and lists recognize GitLab's unique culture and help us spread the word about why this is such a great place to work. Here are the latest:

- [Best Tech Companies For Remote Jobs in 2021 according to Glassdoor, Forbes](https://www.forbes.com/sites/louiscolumbus/2020/12/20/the-best-tech-companies-for-remote-jobs-in-2021-according-to-glassdoor/?sh=2922f81c2ceb)
- [Fortune's Best Small & Medium Workplaces in 2020](https://www.greatplacetowork.com/best-workplaces/smb/2020?category=medium)
- [Great Place to Work Certified, 2020-2021](https://www.greatplacetowork.com/certified-company/7013799)
- [Best Engingeering Departments, Comparably, 2021](https://www.businessinsider.com/companies-with-the-best-engineering-teams-according-to-employees-comparably-2021-4#here-is-the-full-list-of-companies-with-the-best-engineering-departments-26)
- [Best Startup Employers in 2020, Forbes](https://www.forbes.com/americas-best-startup-employers/#46146ae96527)
- [Cloud 100 List, Forbes, 2020](https://www.forbes.com/cloud100/#12ceb6e35f94)
- [No. 2 top private employer via Hired's Brand Health Report, 2020](https://www-forbes-com.cdn.ampproject.org/c/s/www.forbes.com/sites/johnkoetsier/2020/09/22/the-top-40-brands-people-want-to-work-for-in-the-tech-industry/amp/)
- [Happiest Employees, Comparably, 2020](https://www.businessinsider.com/top-companies-employees-happy-fulfilled-comparably-2020-10)
- [Best Perks and Benefits, Comparably, 2020](https://www.businessinsider.com/comparably-big-companies-best-perks-employee-benefits-2020-10)
- [Best Compensation, Comparably, 2020](https://www.businessinsider.com/best-paying-big-companies-comparably-salary-2020-10)
- [Best Work Life Balance, Comparably, 2020](https://www.businessinsider.com/best-companies-if-you-want-to-achieve-work-life-balance-2020-10)
- [Top 100 Remote Work Companies 2020, FlexJobs](https://www.flexjobs.com/blog/post/100-top-companies-with-remote-jobs-2020/)
- [Best Workplaces in 2019, Inc.](/blog/2019/05/16/building-an-award-winning-culture-at-gitlab/)
- [18 Great Companies For Millennials in the San Francisco Area](https://www.comparably.com/articles/18-great-companies-for-millennials-in-the-san-francisco-area/)
- [Best Company Culture, Comparably, 2019](https://www.comparably.com/news/best-company-culture-2019/)
- [Best Companies for Women, Comparably, 2019](https://www.comparably.com/news/best-companies-for-women-2019/)
- [Best Companies for Diversity, Comparably, 2019](https://www.comparably.com/news/best-companies-for-diversity-2019/)

# Articles

Coming soon: A curated list of the latest and greatest articles about GitLab, organized by topic and theme.

For a daily digest of press about GitLab, as well as more detailed regional content, please follow the #newswire channel on Slack.

# Blogs

Coming soon: Our team members have written blogs on numerous topics that are helpful to share with potential candidates and anyone interested in life at GitLab. Here's a curated list of some of the best blog posts to share, organized by topic and theme.

# Videos

There are a number of videos on our [corporate YouTube channel](https://www.youtube.com/gitlab), as well as the GitLab Unfiltered YouTube channel about working at GitLab. Here's a list of some top videos to share.

## General life at GitLab videos

- [Why work remotely?](https://youtu.be/GKMUs7WXm-E)
- [Everyone can contribute](https://youtu.be/kkn32x0POTE)
- [GitLab's core values](https://youtu.be/_8DFFHYAtj8)
- [This is GitLab](https://youtu.be/5QeHmiMFhDE)
- [What is GitLab?](https://youtu.be/MqL6BMOySIQ)
- [Working remotely at GitLab](https://youtu.be/NoFLJLJ7abE)


## Logos, company description, team photos

If you're in need of an official GitLab logo, company description, green screens, or other press-related assets, refer to [the press kit](/press/press-kit/) on the GitLab website.


# Contribute to the content library

This content library is a work in progress. See something that's missing or want to contribute to it? Submit a merge request and tag @bchurch.

[Return to the main talent brand page.](/handbook/people-group/employment-branding/)
