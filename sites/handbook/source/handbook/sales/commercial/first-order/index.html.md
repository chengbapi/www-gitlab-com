---
layout: handbook-page-toc
title: Mid-Market First Order
description: >-
  Mid-Market First Order processes for prospecting, managing, and cultivating accounts.
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Mid Market Key Accounts First Order Mission Statement

The Mid-Market Key Accounts First Order team was started to create and scale the process of landing key new logos for GitLab Mid-Market Sales. We venture to do this efficiently, scalably, and repeatably while fostering the core [CREDIT values of GitLab](https://about.gitlab.com/handbook/values/). As we grow, the goal is to take these processes throughout Commercial and Enterprise Sales.

As a First Order team, we strive to be obsessed with customer success, and aim to sell to IT professionals in a manner that is efficient, comfortable, and honest. We give first and use the resources available to us to create a buying environment that will enable a relationship to foster naturally with GitLab. 

We uphold GitLab brand integrity though our outreach, and we collaborate to ensure we have the skills necessary to execute at the highest level possible. 

## Prospecting Best Practices

## First Order Skills and Skills Development
### First Meeting
### Paid POC
#### **What is a Paid Proof of Concept?**
1. This is part of Two Lane Selling
1. A paid POC is a year-long GitLab subscription we offer prospects we are in active discussions with so that a small segment of their tech team can get started using GitLab Premium or Ultimate immediately. It is at the forefront of what First Order is about as it encourages a quick adoption to demonstrate GitLab's value sooner and enables the Named team to grow the account.
1. We would typically recommend a team of 5-20 people get started on the Paid POC
1. We would typically recommend a team that is embarking on a new project be involved with the Paid POC so that they can start fresh using GItLab for the whole SDLC.
1. The duration is a minimum of 12 months
1. A discount can be applied according to the [discount matrix](https://docs.google.com/document/d/1-CH-uH_zr0qaVaV1QbmVZ1rF669DsaUeq9w-q1QiKPE/edit#). Beware that in order for an opportunity to count as a new logo, it needs to be above $1,000 ARR and have a growth potential of more than 50 seats in the future.

#### **Why recommend the Paid POC to an active opportunity?**
1. A Paid POC promotes a shorter sales cycle as the financial investment is significantly lower and the allocated resources required from the customer to get started are much smaller. It mitigates any perceived risk prospects have to get started. 
1. It typically enables our champion to get started as soon as possible as they will not need budget approval since the investment is typically below the approval threshold.
1. It doesn’t restrict customers to a 1 month trial which many find limiting and therefore postpone initiating it. 
1. It gives customers much needed time to explore the GitLab features and formulate a concise and robust ROI to make internal justification of a wide team adoption easier.
1. It provides the customer with a GitLab Account Manager.
1. It provides them with [Priority Support](https://about.gitlab.com/support/).
1. It can be an exciting initiative for a GitLab champion starting a new project as it empowers them to use GitLab as Single Platform without having to go through all the lengthy approvals which delays the start of their project.
1. You can use [this presentation deck](https://docs.google.com/presentation/d/1MoiL4EkI2goklfSk-rCp_vivbsag9hrEHbd6FVwKy7Q/edit#slide=id.g889353d428_0_0) to help you pitch the Paid POC (particulary refer to slide 9)

### Discovery
### Prospecting
### FORC Calls
1. First Order Reviewed Call (‘FORC’) pronounced Force are Chorus calls First Order AEs review on behalf of their peers to provide feedback and encourage collaboration and skill development.
1. Ideally, AEs should review 1-2 calls per week per AE.
1. The AE should send the call to someone transatlantic to review for early AM local time.
1. The AE who wishes their call to be reviewed should name 1-2 concrete things they were working on during that call and send the Chorus link. Examples include, two lane selling, active listening, price negotiation, GitLab pitch, back of napkin ROI etc.
1. The reviewer should review the call they receive and log their notes in SFDC under the opportunity using the following format “FORC for Company Name” so that we can report on them. 

## Working Cross-Functionally
### Field Marketing
### BDR Team
### Channel

Working with local partners can speed up your sales cycles, increase the overall ARR of your opportunity and help you tackle language barriers.

#### **The process of doing outbound initiatives with Partners** 
1. Familiarize yourself with partners in your territory by searching on GitLab Partner Locator or by speaking to the Channel Manager(s) in your Region.
1. Set up calls with channel partners to understand their business, areas of expertise and how they work with GitLab.
1. Select 2-3 of channel partners per quarter you want to actively work with and arrange bi monthly meetings to strategize on prospect accounts.
    1. To begin, select partners you have already engaged until you are able to better determine which partners are best suited for what service.
    1. You can always consult your Channel manager to advise you on which partners are most active in your segment or are actively working on GitLab initiatives.
1. Prepare a list of prospect accounts you want to outbound (ideally Rank 1 or 1,5 accounts) and share with your preferred partners to see how they can help with prospecting. Partners may have worked with top tier accounts in the past and can provide warm introductions or they can help us with prospecting as some have their own GitLab dedicated BDR teams. 
    1. Work together with your channel manager to help partners arrange webinars, workshops etc.
    1. Promote partners webinars by creating [Outreach Sequences](https://app1a.outreach.io/sequences/7941) to share with prospects.
    1. If you don’t have any contacts on SFDC that you can invite, use a Linkedin Sequence to invite fresh prospects to webinars.

#### **Working on opportunities with Partners** 
1. Familiarize yourself with the GitLab [Partner Program Discount Details](https://docs.google.com/document/d/1lMQHlSMEJQCH1HJnr-MdeVozZ9HJI0oVPmfbPWbJkyw/edit)
1. If a Channel Manager assigns a Deal Registration to your name, ask them to introduce you to the partner, if you don’t already have a relationship with them.
1. Set up a short call with the Partner who registered the opportunity to find out details about the opportunity (use case,pain points, timeline etc). This will help you complete the Command Plan.
1. For opportunities where you introduce a partner as a reseller to the prospect, a co-sell discount will apply. All partners can resell opportunities and they can often do this in the local currency and with more flexible payment terms which is something to consider if your prospect brings up these objections.

### **Important Links**
1. [Details on what our channel program looks like](https://about.gitlab.com/partners/program/)
1. [GitLab Partner Locator](https://partners.gitlab.com/English/directory/)
1. [Program Discount Details](https://docs.google.com/document/d/1lMQHlSMEJQCH1HJnr-MdeVozZ9HJI0oVPmfbPWbJkyw/edit) 
1. [Channel Sales HB Page](https://docs.google.com/document/d/18xqRRCkIXlR7r4BvBQnK9n9zE70q-KPga-lVHhVw4n4/edit) 


### Alliances
### MM-Named/ Territory 
### CS

## How First Order Measures Success

## First Order Logistics

### Rules of Engagement Between MMKAFO and MM-Territory
### MMKAFO to Named Handoff Motion
### Definition of what counts as a logo

## Frequently Asked Questions (FAQ)


