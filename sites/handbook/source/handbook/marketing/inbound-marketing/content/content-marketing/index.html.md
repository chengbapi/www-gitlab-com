---
layout: markdown_page
title: "Content Marketing"
description: "The content marketing team is responsible for strategic and resourceful content creation."
---

## On this page
{:.no_toc}

- TOC
{:toc}

The Global Content team is responsible for strategic and resourceful content creation. The goal of the content marketing team is to drive inbound and organic traffic with SEO by optimizing webpages using keyword clusters, gap analysis research, target keyword updates, and aligning with reader intent. The team provides readers with end-to-end content experiences in order to garner trust, create source credibility, and generate customer engagement.

The content team is using SEO-infused data to create quality definitions, web articles, and topic know-how in order to enhance the existing webpages, add more pages, and drive more traffic to the site. Working closely with the inbound marketing team, the content team has created a content strategy to ensure that people searching for topics like open source, version control, code management, CI/CD, and DevSecOps will find the best possible resources on GitLab’s website.

## Who is our audience?

The content team is specifically addressing the developer audience and those who are committed to learning more about a particular software workflow, application, standard, or methodology. Our content is created to be a trusted source for [Sasha, the software developer](/handbook/marketing/strategic-marketing/roles-personas/#sasha-software-developer) who is looking to advance their career by learning new skills.

The content marketer’s goal is to steer readers through the buyer’s journey -- capture organic traffic through awareness level content and engage readers by providing links to solutions-based knowledge. The content team relies heavily on [user personas](/handbook/marketing/strategic-marketing/usecase-gtm/ci/#user-persona) and [use cases](/handbook/marketing/strategic-marketing/usecase-gtm/), developed by the product marketing team, in order to understand who our readers are and how best to support them in their software development journey. Personas, along with SEO research, help us to navigate content strategies.

## Content creation strategy

We use content pillars and topic clusters to organize and plan our work. Pillars are definied within epics on a quarterly basis and work is broken out into milestones and issues. We use content pillars to plan our work so we can provide great digital experiences to audiences. The content team aligns to themes to ensure we are executing strategically and meeting business goals. Pillars allow us to narrow in on a specific topic and audience, while topic clusters help to inform us of what search terms people are using within that subject. Each content marketer focuses on a specific part of the software development lifecycle, then creates content clusters to support the topic.

### What is a topic cluster?

Topic clusters are pieces of content that share a common subject or subtopic and are used to enhance organic search engine results. A topic cluster begins with a pillar page, consisting of the prominent subject matter. The content team, along with detailed input from inbound marketing, works to design insightful topic clusters. Our [Topics pages](/topics/) act as pillar pages. Web articles, also called cluster pages, will support and enhance the topics pages, using primary keyword placement, search volume research, and semantic and secondary keywords. Our topic clusters are listed below.


![Topic cluster image](/images/handbook/growth-marketing/version-control-cluster.png)


**Existing topic clusters:**
1. **[Version control & collaboration](https://docs.google.com/spreadsheets/d/1kUvj1liGbBb3cOnYqhnPGBgKNGAKO-4guwJ6LqUlSSU/edit?usp=sharing)**
2. **[Continuous integration](https://docs.google.com/spreadsheets/d/1eqFcadC0zoUn7bB9r76bBmNylMcB1ifALq_Ij-Hwpog/edit?usp=sharing)**
3. **[DevSecOps](https://docs.google.com/spreadsheets/d/13w0u994Y19RGxC6Nz3xhd55CBf--lWr_EBhj76oHgSg/edit?usp=sharing)**

### What is a content pillar?

A content pillar is a go to market content strategy that is aligned to a high-level theme executed via content sets. Often, content pillars are defined based on [value drivers and customer use cases](/handbook/marketing/#Go_to_market:_value_drivers and_customer_use_cases) in order to support go to market strategy. For example, "Just commit to application modernization" is a content pillar about improving application infrastructure in order to deploy faster. Within this pillar, many topics can be explored (CI/CD, cloud native, DevOps automation, etc.) and the story can be adapted to target different personas or verticals. Each set created should produce an end-to-end content experience (awareness to decision) for our audience.

![Content pillar diagram](/images/handbook/marketing/corporate-marketing/content-pillar.png)


#### What's included in a content set?

A content set is the collection of work that is be created each quarter. It covers all three stages of the buyers journey. The content marketers are responsible for awareness level and some consideration level content, and the product marketers cover the purchase and some consideration content.

Here's an example of what's included in a content set:

| Stage | Content Type | DRI |
| ------ | ------ | ------ | ------ |
| Awareness | Thought leadership blog post | Content marketing |
| Awareness | Topic page | Content marketing |
| Awareness | Web article | Content marketing |
| Awareness | Resource | Content marketing |
| Consideration | Case study | Content marketing |
| Consideration| Technical blog post | Content marketing |
| Consideration | Whitepaper | Product & technical marketing |
| Consideration | Solution page | Content & product marketing |
| Consideration | Webcast | Product & technical marketing |
| Purchase | Demo | Technical marketing |
| Purchase | Data sheet | Product marketing |

**Sources:**

- [What Is a Content Pillar? The Foundation for Efficient Content Marketing](https://kapost.com/b/content-pillar/){:target="_blank"}
- [How to Create Pillar Content Google Will Love](https://contentmarketinginstitute.com/2018/04/pillar-content-google/){:target="_blank"}
- [What Is a Pillar Page? (And Why It Matters For Your SEO Strategy)](https://blog.hubspot.com/marketing/what-is-a-pillar-page){:target="_blank"}
- [Content for Each Buying Stage of the Consumer Purchase Cycle](https://contentwriters.com/blog/content-consumer-purchase-cycle/){:target="_blank"}
- [AlsoAsked.com](https://alsoasked.com/){:target="_blank"}

## Meet the Content Marketing team

**[Sharon Gaudin](https://gitlab.com/sgaudin)**

Title: Sr. Content Marketing Manager

Area of focus: DevOps

GitLab handle: @sgaudin

Slack handle: @Sharon Gaudin

**[Kristina Weis](https://gitlab.com/KristinaWeis)**

Title: Sr. Content Marketing Manager

Area of focus: CI/CD

GitLab handle: @KristinaWeis

Slack handle: @Kristina Weis


### Recommended Slack channels

Anyone at GitLab can reach out to the content team by tagging `@contentteam` on Slack.

This is a list of recommended slack channels for the content marketing team.

* **`#content-team-lounge`**: Where the content team can hang out. This is a public channel.
* **`#content`**: A general discussion channel for all things content, including blog posts, case studies, videos, webcasts, newsletter content, interesting articles, etc
* **`#content-updates`**: A rolling log of new, published content. Once an article, blog post, case study, webinar, video, landing page, etc. is public, add it to this channel with a TL;DR description of the content.
* **`#inbound-mktg`**: The main channel for the Inbound Marketing group that includes content marketing.
* **`#marketing`**: The main channel for the entire marketing group that includes inbound marketing.


## Planning timeline

Pillar strategy is planned annually and reviewed quarterly. Content sets are planned quarterly and reviewed monthly. When planning, we follow the 80/20 rule: 80% of capacity is planned leaving 20% unplanned to react to last minute opportunities.

- **2 weeks prior to start of the quarter:** Proposed content plans are published to the content schedule. Product marketing, digital marketing, and corporate events marketing give feedback on the plan.
- **1 week prior to the start of the quarter:** Kickoff calls are held.
- **1st day of the quarter:** Content plans are finalized.
- **1st of each month:** Progress reviews are held. Plans are adjusted if needed.
- **Last day of the quarter:** Cross-functional retrospective is held.

## Gated asset creation workflow
Gated assets are created and written by the content team. The process will vary, however the content strategy and timeline should be consistent. Several other teams are involved in the planning process for whitepapers and eBooks. To learn more about the campaign and marketing strategy, the [Marketing Programs Management section](/handbook/marketing/revenue-marketing/digital-marketing-programs/marketing-programs/gated-content/#overview)) of the handbook has an overview.

All gated assets should have an epic. There are many teams involved in creating the final product and an epic helps keep al realted issues together. The epic is the single source of truth for workflow deadlines. Gated assets include eBooks, whitepapers, demos, and webcasts. All gated assets can be found on the [Resources](/resources/) page.

Visit the [Gated Content page](/handbook/marketing/revenue-marketing/digital-marketing-programs/marketing-programs/gated-content/#gated-content) for detailed instructions on [how to structure your epic, issues, and timeline.](/handbook/marketing/revenue-marketing/digital-marketing-programs/marketing-programs/gated-content/#timeline-and-process-alignment)

### Content types: Definitions and workflows

Content is most effective when it has the right message, to the right audience, at the right time, in the right medium. That's why the content marketing team produces a variety of content types designed to reach different people at different stages of the buyer journey.

At GitLab, everyone can contribute. While the content marketing team produces a large amount of content, they are by no means the only content creators at GitLab. If you would like to contribute content such as blog posts, eBooks, whitepapers, etc, read more about these content types and how to submit them.

#### Blog post

A post on the GitLab blog is used during the Awareness or Consideration stage of the buyer's journey. A blog post can educate, entertain, tell a story, take an opinionated stance, etc. A blog post is dated, so it only reflects thoughts, ideas, and processes from a specific period of time. For communicating long-term/evergreen ideas or processes, consider using a [topic page](/handbook/marketing/inbound-marketing/content/content-marketing/#topic-page) or [web article](/handbook/marketing/inbound-marketing/content/content-marketing/#web-article) instead.

**Content Marketer workflow**
1. To suggest a blog post, follow the **[How to pitch a blog post](/handbook/marketing/blog/#how-to-suggest-a-blog-post)** process in the handbook. It's ideal to do this as far in advance of when you hope to publish as possible, so there's plenty of time to discuss the ideal angle or approach to optimize for GitLab's blog audience. Be sure to include any context the Editorial team might need when reviewing your pitch (e.g. Is the post supporting a campaign? Is there a specific reason why you chose this topic? Does anyone else need to approve the post before we can go ahead?) and alert them if the blog post needs to be published on a specific date (to support an event or campaign, for example).
2. Once the Editorial team has provided feedback and approved the blog topic, consider opening an issue in the [Content Marketing project](https://gitlab.com/gitlab-com/marketing/inbound-marketing/global-content/content-marketing/-/issues/new?issue%5Bassignee_id%5D=&issue%5Bmilestone_id%5D=) to capture your work during the writing and collaboration process (See: [Every task should be an issue](/handbook/marketing/project-management-guidelines/issues/#every-task-should-be-an-issue)).
3. Once blog copy has been reviewed by the required SMEs/strategic marketing counterparts, close the Content Marketing issue, [create a merge request](/handbook/marketing/blog/#when-youre-ready-to-create-your-blog-post) from your www-gitlab-com issue, and when ready, assign it to the Editorial team member who reviewed your pitch. They will then schedule your blog post for the next available date on the [Editorial calendar](/handbook/marketing/inbound-marketing/content/#editorial-calendar).
1. The Editorial team member will review your draft and may leave feedback for you to resolve before publishing. When you have done so, reassign the MR to the Editorial team member for final review and merging.

Blogs can be pitched to the [official GitLab blog](/blog/){:target="_blank"} or published directly on the [Unfiltered blog](/blog/categories/unfiltered/){:target="_blank"} without editorial approval.

Visit the blog handbook to learn more about the [blog publishing process](/handbook/marketing/blog/){:target="_blank"}.

#### Blog repurpose

A blog can and should be repurposed as a web article/cluster page if the content is evergreen and awareness stage. The blog should also be older than six months and not GitLab-centric.

**Blog repurpose workflow**
1. Work with the SEO team to identify existing, older blogs that are the best options for repurposing.
2. Ensure that the editorial team is aware of the intention and has approved the redirect.
3. In a Google doc, copy and paste the existing blog. From there, use the keyword spreadsheets to identify the best possible updates -- this can include changing the title to a better keyword phrase, identifying areas of copyediting improvements, adding links, adding a 'More On' section, and elaborating on longtail keywords throughout the content.  
5. Create a merge request on the `www-gitlab-com` project.
6. Once the web article is edited and merged, open an [issue to redirect](https://gitlab.com/gitlab-com/marketing/inbound-marketing/growth/-/issues/new#set-up-a-new-redirect) the blog URL to the new web article. Add the new URL and old URL in the issue. The labels are included in the issue, do not assign.
7. After the web article is merged and the blog redirected, add the web article link to the associated Topics page in the sidebar.

#### Whitepaper

A whitepaper is a technical and focused topic study intended to educate a prospective buyer during the Consideration or Purchase stages of a campaign. The whitepaper offers a problem and solution instance in a granular, technical tone. The content team member should collaborate closely with their product marketing counterpart when researching and writing the asset so that the content reaches appropriate technical standards for the intended audience.

Any technical GitLab team member is welcome to write a whitepaper and collaborate with the content team. Go to the [product stage](/handbook/product/categories/#devops-stages) in the handbook to find the content marketer for your whitepaper topic, or post in the [#content](https://gitlab.slack.com/archives/C2R1NED61){:target="_blank"} slack channel. Whitepapers should be related to specific use cases and support campaigns, when possible.

**Examples:**
1. [A seismic shift in application security](/resources/whitepaper-seismic-shift-application-security/)
2. [How to deploy on AWS from GitLab](/resources/whitepaper-deploy-aws-gitlab/)

**Timeline:** 1-4 weeks to plan, research, and draft a whitepaper. Whitepapers created by GitLab team members will follow the [internal gated content](/handbook/marketing/revenue-marketing/digital-marketing-programs/marketing-programs/gated-content/#internal-content-created-by-the-gitlab-team) asset workflow.

**Deliverables:**
* Week 1: Create and share a detailed outline of the whitepaper with involved team members.
* Week 2: Share a first draft with the team and include review parameters and deadlines.
* Week 3: PMM and subject matter expert (SME) reviews are completed. The whitepaper content track changes are reviewed/accepted by the content DRI and shared with content team reviewer(s).
* Week 4: Reviews are completed. You may need an additional week and second round of reviews if significant content changes are made.

#### eBook

An eBook tends to be broader in scope than a whitepaper and provides a clear definition of the topic, along with various industry standard best practices. eBooks typically provide awareness-level content, but can dive deeper into GitLab if it's intended for late-stage consumption. eBooks should be related to specific use cases and support campaigns, when possible.

**Content Marketer workflow**

A content team member develops eBook content with input and review from their product marketing counterpart. More technical or instructive eBooks may require more collaboration with product marketing. eBooks follow the [internal gated content](/handbook/marketing/revenue-marketing/digital-marketing-programs/marketing-programs/gated-content/#internal-content-created-by-the-gitlab-team) asset workflow.

Follow the instructions for creating an internal gated asset and start the eBook process by creating an epic using the [epic template](/handbook/marketing/revenue-marketing/digital-marketing-programs/marketing-programs/gated-content/#organizing-content-pillar-epics-and-issues).

**For other GitLab team members:** To request an eBook from the content team, feel free to open a [content request](https://gitlab.com/gitlab-com/marketing/inbound-marketing/global-content/content-marketing/-/issues/new?issue%5Bassignee_id%5D=&issue%5Bmilestone_id%5D=#) issue. If you would like to write an eBook, we have an [eBook google doc template](https://docs.google.com/document/d/1V944SVbjBxUVMtZiWIS-Baoxc7Ui-gjq3fCYf8tK0D4/edit?usp=sharing) available for GitLab team members to use with general best practices.

**Examples:**
1. [An intermediate guide to GitOps, Kubernetes, and GitLab](https://page.gitlab.com/resources-ebook-gitops-kubernetes-gitlab.html)
2. [The GitLab Remote Playbook](/company/culture/all-remote/)
3. [The benefits of single application CI/CD](/resources/ebook-single-app-cicd/)

**Timeline:** 1-3 weeks to plan, research, and draft an eBook. All eBooks should have a launch date on the [editorial calendar](/handbook/marketing/inbound-marketing/content/editorial-team/#blog-calendar).

**Deliverables:**
* Week 1: Share a detailed outline with involved team members.
* Week 2: Share a first draft with the team and include review parameters and deadlines.
* Week 3: Reviews are completed. You may need an additional week and second round of review if significant content changes are made.

#### Infographic

An infographic is an illustrated overview of a topic or process, and is typically an ungated asset. Infographics should tell a story using data, diagrams, and text. It can be used to discuss industry trends, relate insights, or explain different stages of a project. Open a [design request issue](https://gitlab.com/gitlab-com/marketing/corporate_marketing/corporate-marketing/-/issues/new?issuable_template=request-design-general) when planning an infographic to figure out a structure for the graphic, and ask the design DRI for recommended copy lengths.


**Example:**
1. [Git cheat sheet](/images/press/git-cheat-sheet.pdf)
2. [How GitLab accelerates workload deployments on AWS](https://gitlab.com/gitlab-com/marketing/inbound-marketing/growth/uploads/59d1390557bb304e7401443c4e710c0f/gitlab-aws-ci_t-campaign-infographic-01.pdf)

#### Topic page

A topic page is a high-level explanatory "pillar" page dedicated to a specific topic, such as [version control](/topics/version-control/), [DevSecOps](/topics/devsecops/), or [continuous integration](/topics/ci-cd/). Topic pages should explain what the subject is, why it is important, and explain the basic concepts of the subject. Topic pages should include links to additional related resources, such as blogs, web articles, videos, and case studies, as well as at least one CTA to a gated asset.

#### Web article

Web articles are educational, informational content, designed to support topic pages using keywords and search terms. They are similar to blogs in length, but differ in that they are not dated and the content is evergreen (see [more about blog posts](#blog-post)). Web articles should be linked on one or more topic pages, and should serve as a deep-dive into a specific sub-topic. Web articles are created in accordance with SEO research and campaign goals that are listed in the topics clusters spreadsheets. Complete the [SEO web article template](https://docs.google.com/document/d/1ON2CZ7uufl9gtqh-P67AxU5Y42ctQ3mRtPsgJRlep7Y/edit?usp=sharing) when creating a new piece of content.

**Examples:**
1. [How to choose the right continuous integration tool](/topics/ci-cd/choose-continuous-integration-tool/)
2. [What are Git version control best practices?](/topics/version-control/version-control-best-practices/)
3. [What is developer-first security?](/topics/devsecops/what-is-developer-first-security/)

#### Case study

Case studies are in-depth customer stories that provide insight as to how GitLab has resolved significant software workflow problems for a company. The case study tells the story using quotes from customer interviews and straightforward metrics that broadly show the impact of adopting GitLab.

[Case studies](/customers/) are created in partnership with the customer reference team. The customer reference team has a process in place for how they add new references and provide a list of [customer value drivers](/handbook/marketing/product-marketing/customer-reference-program/customer-insight/#command-of-message-questions). The content team is responsible for writing the case study asset using [this template](https://docs.google.com/document/d/1UbhW2AEP7BfEZJGcp7w4LS6-W-xSeGET4K4NDr7bP0E/edit?usp=sharing), [publishing the case study](/handbook/marketing/product-marketing/customer-reference-program/customer-insight/#publishing-to-the-website) to the website, [adding the customer logo](/handbook/marketing/product-marketing/customer-reference-program/customer-insight/#adding-customer-logo-and-case-study-to-customers-grid) to the customer logo grid, and ensuring that the case study goes through proper social media and newsletter steps.

**Examples:**
1. [How Hotjar deploys 50% faster with GitLab](/customers/hotjar/)
2. [Axway aims for elite DevOps status with GitLab](/customers/axway-devops/)
3. [Security provider KnowBe4 keeps code in-house and speeds up deployment](/customers/knowbe4/)

### Content set template

```
## Persona: [Example: Software developer Sacha](https://about.gitlab.com/handbook/marketing/product-marketing/usecase-gtm/ci/#software-developer-sacha)

## Use Case: [Example: Continuous integration](https://about.gitlab.com/handbook/marketing/product-marketing/usecase-gtm/ci/)

## Theme

*Detail the specific angle you are taking on the topic. Why does it matter to this persona and what core messages need to be conveyed?*

## Strategic overview and resources

*Why are we targeting this theme and persona?*

-  [Industry analysts](https://about.gitlab.com/handbook/marketing/product-marketing/usecase-gtm/ci/#industry-analyst-resources)
-  [Market requirements](https://about.gitlab.com/handbook/marketing/product-marketing/usecase-gtm/ci/#market-requirements)
-  [Discovery questions](https://about.gitlab.com/handbook/marketing/product-marketing/usecase-gtm/ci/#discovery-questions)

## Content Audit

1. Link to Path Factory use case track
1. Link to blog topic tag
1. Link to resources page filtered by topic
1. Link to topic page
1. Link to solutions page
1. Link to related /stages-devops-lifecycle/ pages

## PathFactory track

       1. What content exists
       1. What content needs to be updated?
       1. What content is missing?

## SEO

*List the primary and secondary keywords for this content set.*

*Resources*
- [SEO content manual](https://docs.google.com/document/d/1ZPMW2ypTy2RssojVsulEkk1O_mLuM26liUU-Qoj-ACE/edit?usp=sharing)
- [AlsoAsked.com](https://alsoasked.com/)
- [SEMrush](https://www.semrush.com/)
- [Answer The Public](https://answerthepublic.com/)

## Content set

*What content are you planning to create/update and why?*

- Topic maturity assessment -- Following the template of the [DevSecOps maturity assessment](https://about.gitlab.com/resources/devsecops-methodology-assessment/), we'll create a module for each topic. Ideally, this will be interactive for the user and then align with the buyer's journey to provide content according to where they land on the assessment.

- eBook

      1. Focus/Title

- Web article

      1. Focus/Title
      2.
      3.
      4.
      5.

- Topic page

       1. Review/update/add - keep your topic page on your radar to continuously iterate.

## Timeline

*List content with phases and due dates*

- Maturity assessment
      1. Due date will likely be determined by design planning timeline.

- eBook

      1. Due date

- Web articles

      1. Due date
      2.
      3.
      4.
      5.

- Topic page

       1. How often will you update (i.e. 2/month, etc)

```


### Epic template

```
**Epic title:** [asset type] Working Title - Launch date

## Launch date: TBD

* Content DRI:   
* MPM DRI:

### Deadlines

* [ ]  2020-XX-XX Open copy issue and complete asset brief (-27 business days from launch)
* [ ]  2020-XX-XX Landing page copy due (-27 business days from launch)
* [ ]  2020-XX-XX Final copy due  (-15 business days from launch)
* [ ]  2020-XX-XX Final design due (-5 business days from launch)
* [ ]  2020-XX-XX MR ready to launch (-3 business days from launch)
* [ ]  2020-XX-XX Asset added to pathfactory (-3 business days from launch)
```
