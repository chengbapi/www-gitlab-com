---
layout: handbook-page-toc
title: Expenses
---
<link rel="stylesheet" type="text/css" href="/stylesheets/biztech.css" />

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}


## <i class="far fa-paper-plane" id="biz-tech-icons"></i> Introduction

Welcome to the Expenses page! You should be able to find answers to most of your questions here. If you can't find what you are looking for, then:

- **Email**: `expenses@gitlab.com`
- [**FAQ Document**](https://docs.google.com/document/d/1F5YmkJCOu5vNRbVdgVf1CaXdqgo8VDk1-MGLKcBkOAI/edit?usp=sharing)

GitLab utilizes Expensify as our team member expense reimbursement tool. All team members will have access to Expensify within 2 days from their hire date. If you didn't receive an email from Expensify for your access, please contact the AP team through one of the channels listed above.
**Note**: Ensure to opt-out from news by logging in and navigating to `Settings > Preferences > Contact Preferences` and un-checking the box at `Relevant feature updates and Expensify news`.

Some entities also use Tipalti for reimbursement. If you are in BV, Ireland, GmbH, GK or PTY Ltd you will also receive an invite from Tipalti to set up your banking details.

Expense reports are to be submitted at least once a month and must be submitted within 90 days of the invoice date, service date or purchase date. Additional information on getting started with Expensify and creating/submitting expense reports can be found [here](https://community.expensify.com/discussion/5922/deep-dive-day-1-with-expensify-for-submitters/p1?new=1).

The procedure by which reimbursable expenses are processed varies and is dependent on contributor legal status (e.g. independent contractor, employee) and [subsidiary assignment](https://about.gitlab.com/handbook/tax/#tax-procedure-for-maintenance-of-gitlabs-corporate-structure). Check with Payroll if you are unsure about either of these.

Team members should also consider the terms and conditions of their respective contractor agreements when submitting invoices to the company.

Team members are reimbursed on different schedules depending on their location. Please see the reimbursement process below to find the listing of reimbursement dates based on your location or employment status.

## <i class="far fa-flag" id="biz-tech-icons"></i> Expense Policy Summary

1. Max Expense Amount - [$5,000 USD](https://www1.oanda.com/currency/converter/). **NOTE** - If you are a Corporate Credit Card holder, please refer to the [Corporate Credit Card page](https://about.gitlab.com/handbook/finance/accounts-payable/corp-credit-cards/) for specific instructions and thresholds.
1. Itemized receipts are required for all business related expenses that are greater than $75 USD or equivalent to local currency.
1. Expenses should be submitted within 90 days of purchase, spend, or invoice due date. This helps the company to better manage our budget and ensures accurate month-end reporting.
   - If your purchase requires that you prepay for services, such as quarterly or yearly payments, you must split the cost monthly and only submit 1 month (or 1 payment) at a time for reimbursement.
   - If you are required to pay 250 every 3 months, divide the total cost by the # of months and that is what you submit each month.
   - You can use the same invoice copy each month.
1. Expense report items should clearly state if the spend includes amounts for clients / non-team members. Tax requirements in certain countries require us to account for client spend differently.
1. All team members must submit their expense reports in their designated policies in Expensify - COGS team members in COGS policies and non-COGS team members in non-COGS policies.
   - You can verify the policy that you are reporting in by review the title of your report: `Report Title: Team Member Name | Receipt Date: YYYY-MM-DD to YYYY-MM-DD | **Policy Name: GitLab- Amex** | Report ID: ######## | Total $XXX.XX`
   - The value next to *Policy Name* should match the entity, PEO, Country and COGS/Non-COGS department that you are employed with.  If these values are not a match, you can change them on the right hand side of the page - under Details, then under policy there is a drop down to choose from any policies you are a part of.
1. Gift cards are not accepted as a form of payment for business expenses.
1. All internet/telephone reimbursements must have a detailed receipt attached which shows the amount, service, and service date.
   - Please note we do not reimburse late fees, initial set up, or equipment costs.

#### <i class="fa-solid fa-arrow-right"></i> [**Approved List of Expenses**](https://docs.google.com/document/d/1UIbpeEzGQ0kmv9uI47xP8hRfDpGnwxvIeUpR66j_Ox8/edit?usp=sharing)


### Domain Name Registration and Maintenance Policy

#### Scope and Purpose
This policy applies to all GitLab team members registering, or maintaining a registration for, GitLab-related Domain Names, including GitLab-related Domain Names registered or used for sandbox or testing purposes.

Domain names are key assets in GitLab's intellectual property portfolio. Centralizing the registration and maintainance of domain names under the Infrastructure Shared Services group using GitLab's approved domain name registrar helps us track and protect these valuable assets.

"**GitLab-related Domain Names**" when used in this policy means any domain name:
   - registered or used for any purpose related to a team member's role at GitLab;
   - containing the GitLab trademark (GITLAB) or one of its derivatives (like, amongst other things, git, glab, gtlb, gl); and/or
   - containing any GitLab key messaging term (like, amongst other things, devops, devops platform, all remote).
   
#### Registration of new GitLab-related Domain Names
- All GitLab-related Domain Names must be registered using the process outlined in the [dns-domain-purchase-request issue template](https://gitlab.com/gitlab-com/business-technology/engineering/infrastructure/issue-tracker/-/issues/new?issuable_template=dns_domain_purchase_request) maintained by IT Operations.
- Expense reports submitted by team members for expenses incurred on or after `2022-03-01` in registering new GitLab-related Domain Names will be rejected.

#### Transfer of existing GitLab-related Domain Names
- All GitLab-related Domain Names currently registered in the name of team members, or registered with unapproved registrars, should be transferred to GitLab using the process outlined in the [dns-domain-transfer-request issue template](https://gitlab.com/gitlab-com/business-technology/engineering/infrastructure/issue-tracker/-/issues/new?issuable_template=dns_domain_transfer_request) as soon as possible.
- Expense reports submitted by team members for expenses incurred on or after `2022-08-01` in maintaining registrations of GitLab-related Domain Names in the name of team members, or with unapproved registrars, will be rejected.

### Transport/Delivery of Second Hand and Free Procurements
Feel free to check local second-hand/free markets when looking for equipment, especially furniture such as desks and chairs. GitLab will reimburse the cost of any transport and delivery services you need to procure the item(s) provided the total cost is reasonable based on the table in the [Office Equipment and supplies page](/handbook/finance/procurement/office-equipment-supplies/#home-office-equipment-and-supplies), factoring in any local pricing adjustments.

If you are able to find hidden gems in these markets, please go ahead and make the purchase. In order to expense these types of purchases, we ask that you take a screenshot or photo of the sale listing and any fund transfers or conversations between yourself and the seller on price and delivery costs. 

### Team Building Budget

In FY23, each eGroup member has been allocated $50 per team member per quarter for FY23-Q1 to FY23-Q3 for team building events. For FY23-Q3 each eGroup member may determine if they wish to use their team building allocation at Contribute or for a different team building event. There is no additional team building budget for Contribute above the $50 per team member. In FY23-Q4 there is an additional budget of $100 per team member. More to come in the section below as we get closer to FY23-Q4.

### <i class="fas fa-bullseye" id="biz-tech-icons"></i> Year-end Holiday Party Budget

GitLab has allocated [$100 USD](https://www1.oanda.com/currency/converter/) per GitLab team member for a holiday event in December. More guidance to come as we get closer to FY23-Q4.

### Virtual Meal with GitLab Team member(s)

Currently Suspended

## <i class="far fa-flag" id="biz-tech-icons"></i> Approving Expense Reports

1. All expense reports are approved by the team members direct manager or their designated approver when they are out of the office.
1. It is expected that the expense report approver will perform a complete review to ensure the reasonableness and accuracy of the submitted expenses.
1. Expensify will send a notification email to the designated approver when a team member submits an expense report.
    - Click on the report name in the body of the email
    - Review each expense for the correct amount of the receipt and the report
    - Check for customers or project name if applicable under Tag
    - We required a receipt for any expense greater than $75
    - Select [Approve and Forward] option and Expensify pre-populated the email address. Note, Expensify is updating their coding to address a small glitch in this field. If it is empty, please send it to **Montpac** ([gitlab-expensify-mp@montpac.com](mailto:gitlab-expensify-mp@montpac.com))
    - **Important** - please do not use [Final Approval] because Expensify will not send the email notification for payment approval and it will delay the reimbursement process
    - Manager can delegate the approval process during PTO:
    - Settings
    - Your Account
    - Vacation Delegate
    - Enter the email address of the backup approval
    - All expense question(s) can be addressed by emailing `expenses@gitlab.com`.

### Reimbursement Payout Timelines:
#### SafeGuard

The list of SafeGuard countries can be found [here](https://about.gitlab.com/handbook/people-group/employment-solutions/#peo-professional-employer-organization-employer-of-record-and-not-a-gitlab-entity)
* Team members who are employed through SafeGuard must submit their expense for reimbursement through the respective country's policy in Expensify. 
* All Expense Reports must be approved by the Manager, and Montpac by EOD on the 7th of the month so that AP can provide the listing of approved reports to Payroll to generate the payment in that month's deposit.

#### Global Upside & Remote.com

The list of Global Upside & Remote countries can be found [here](https://about.gitlab.com/handbook/people-group/employment-solutions/#peo-professional-employer-organization-employer-of-record-and-not-a-gitlab-entity)
* Team members must submit their expenses through Expensify. 
* All expense reports must be submitted and approved by manager and the Accounts Payable team by the 7th of the month to include in the current month payment.

#### iiPay

* All Individual contractors or C2C, with exception of Nigeria will be reimbursed by iiPay by the 22nd of each month.  All expense reports must be approved by the manager and the Accounts Payable team by the 8th of each month to be included in the current month payment.  For contractors with C2C status, be sure to contact the Payroll team via email at `nonuspayroll@gitlab.com` and `expenses@gitlab.com` if you need to set up a separate bank for your expense reimbursement.

#### Legal entities:
* Expense reports for GitLab Ltd (UK) must be approved by the manager and the Accounts Payable team on or before the 13th of each month in order for the reimbursement to be included in the current month payroll.
* Expense reports for GitLab Canada Corp must be approved by the manager and the Accounts Payable team before the 1st day of each payroll period.  Please see [Payroll Calendar](https://docs.google.com/spreadsheets/d/1ECkI_Z8R82j1eipJEEybXjO-EDtzw4TuhJPOnHypDho/edit#gid=0) for the payroll cut off date.
* Expense reports for GitLab France S.A.S must be approved by the Manager and the Accounts Payable team on or before the 7th of each month in order for the reimbursement to be included in the current month's Payroll.
* Expenses reports for GitLab Korea Limited must be approved by the Manager and the Accounts Payable team on or before the 7th of each month in order for the reimbursement to be included in the current month's Payroll.
* Expense reports for Singapore PTE. LTD. must be approved by the Manager and the Accounts Payable team on or before the 7th of each month in order for the reimbursement to be included in the current month's Payroll.
* Expense reports for GitLab BV Finland must be approved by the Manager and the Accounts Payable team on or before the 7th of each month in order for the reimbursement to be included in the current month's Payroll.
* Expense reports for GitLab BV (Belgium and Netherlands), GitLab GmbH (Germany), GitLab PTY Ltd (Australia and New Zealand), GitLab GK (Japan), and GitLab LTD (Ireland) are reimbursed via GitLab AP within 10 business days from the approval date by their manager and the Accounts Payable team.
* Expense reports for GitLab Inc, GitLab Inc Billable, and GitLab Federal reimbursed via Expensify, within 5 business days after the final approval is completed by AP or Montpac.
    - The team member's bank account must be set up in Expensify in order for payment to complete. If you are unsure how to set up your bank please reference the FAQ document linked at the top of this page.

#### Nigeria

* Please include your expenses along with receipts on your monthly salary invoice.

#### CXC Global

The list of CXC countries can be found [here](https://about.gitlab.com/handbook/people-group/employment-solutions/#peo-professional-employer-organization-employer-of-record-and-not-a-gitlab-entity)
* Expense reports must be submitted in Expensify by team members, approved by their managers, and final approved by Accounts Payable team on or before the 7th of each month to ensure it is included in the current months payroll
* GitLab Payroll will send the approved expense amount to CXC EMEA payroll to include with the monthly salary
* Team members must include the approved expense amount on their monthly invoice as well.

## Team Member Expense Temporary Advances

These instructions apply if a team member is unable to purchase required items, for whatever reason.

1. A request is sent to Payroll explaining the reason for the advance. uspayroll@gitlab.com or nonuspayroll@gitlab.com
2. [Expense reimbursement template](https://docs.google.com/spreadsheets/d/1D0kWlqol7jBjqn7yDY6uc7UCWSIcUmiF/edit?usp=sharing&ouid=108533621432009168804&rtpof=true&sd=true) is filled out and returned to Payroll.
    1. Must include the correct entity, currency, VAT, valid receipts and banking details for payment.
3. Payroll reviews and approves/rejects. 
    1. If approved, they forward the report to the VP, Corporate Controller, or Principal Accounting Officer to request approval for reimbursement. 
4. Once approved, payroll forwards the approval and reimbursement request to AP. 
    1. The request must include valid banking details for the individual to receive payment.
5. AP will do their best to pay the reimbursement within 1 week, depending on the date submitted. 
    1. Note that AP completed payments on Thursdays unless otherwise instructed for month and quarter end timelines.

## Spend reduction

When reducing spend, we'll not take the easy route of (temporarily) reducing discretionary spending.
Discretionary spending includes expenses like travel, conferences, gifts, bonuses, merit pay increases and Contributes.
By reducing in these areas we put ourselves at risk of [increasing voluntary turnover among the people we need most](https://steveblank.com/2009/12/21/the-elves-leave-middle-earth-%E2%80%93-soda%E2%80%99s-are-no-longer-free/).
Discretionary spending is always subject to questioning, we are frugal and all spending needs to contribute to our goals.
But we should not make cuts in reaction to the need to reduce spend; that would create a mediocre company with mediocre team members.
Instead, we should do the hard work of identifying positions and costs that are not contributing to our goals.
Even if this causes a bit more disruption in the short term, it will help us ensure we stay a great place to work for the people who are here.

## Expense Reimbursement for Terminated Team Members

If a team member whom you managed has left GitLab and comes to you with final expenses that are valid for reimbursement, please verify that they were not already processed in Expensify and then contact Accounts Payable. You must submit valid receipts and a completed copy of the [Expense Reimbursement template](https://docs.google.com/spreadsheets/d/1D0kWlqol7jBjqn7yDY6uc7UCWSIcUmiF/edit?usp=sharing&ouid=108533621432009168804&rtpof=true&sd=true) along with your approval. Please note that we may also ask the terminated team member to provide valid banking details in order to process the payment to them. 
AP will do their best to process and pay the reimbursement to the individual within 1 week.

## Rejected Expenses

Sometimes expenses get rejected by our third party accounting team (Montpac). Please ensure the expenses you are asking to reimburse are covered in this policy before purchasing. For example, an expense might get rejected if it is more than 20% higher than the guideline price listed in our [office equipment and supplies](/handbook/finance/procurement/office-equipment-supplies/) handbook page.

- - -

Return to the main [finance page](/handbook/finance/).
