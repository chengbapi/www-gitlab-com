---
layout: handbook-page-toc
title: "Product Design Pairs"
description: "Product designer pairs rotation schedule"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

This is the rotation schedule for FY23-Q1 and Q2 (February - July, 2022). Learn more about [Pair Designing](/handbook/engineering/ux/how-we-work/#pair-designing).

[//]: # TIP: Create the schedule in a temporary spreadsheet, and then copy/paste the rows into an online markdown generator (https://www.google.com/search?q=copy-table-in-excel-and-paste-as-a-markdown-table)

| Product Designer       | Time Zone | Design Pair                 |
|------------------------|-----------|-----------------------------|
| Katie Macoy            | GMT+13    | Amelia Bauerly              |
| Michael Le             | GMT+11    | Libor Vanc                  |
| Nadia Sotnikova        | GMT+8     | Jeremy Elder                |
| Veethika Mishra        | GMT+5     | Matej Latin                 |
| Matej Latin            | GMT+2     | Veethika Mishra             |
| Ali Ndlovu             | GMT+2     | Sunjung Park                |
| Sunjung Park           | GMT+1     | Ali Ndlovu                  |
| Daniel Mora            | GMT+1     | Andy Volpe                  |
| Kevin Comoli           | GMT+1     | Dan Mizzi-Harris            |
| Camellia Yang          | GMT+1     | Matt Nearents               |
| Dan Mizzi-Harris       | GMT+1     | Kevin Comoli                |
| Sascha Eggenberger     | GMT+1     | Mike Nichols                |
| Pedro Moreira da Silva | GMT+0     | Gina Doyle                  | 
| Gina Doyle*            | GMT-5     | Pedro Moreira da Silva      |
| Austin Regnery         | GMT-5     | Annabel Gray                |
| Mike Nichols           | GMT-5     | Sascha Eggenberger          |
| Andy Volpe             | GMT-5     | Daniel Mora                 |
| Michael Fangman        | GMT-5     | Emily Sybrant               |
| Emily Bauman           | GMT-5     | Becka Lippert               | 
| Emily Sybrant          | GMT-6     | Michael Fangman             |
| Becka Lippert          | GMT-6     | Emily Bauman                |
| Alexis Ginsberg        | GMT-6     | Nick Brandt                 |
| Jeremy Elder           | GMT-6     | Nadia Sotnikova             |
| Annabel Gray           | GMT-6     | Austin Regnery              |
| Nick Brandt            | GMT-7     | Alexis Ginsberg             |
| Matt Nearents*         | GMT-7     | Camellia Yang               |
| Amelia Bauerly         | GMT-8     | Katie Macoy                 |
| Libor Vanc             | GMT-8     | Michael Le                  |

### *Temporary pairing

The following Product Designers are temporarily paired while their original pairs are on extended PTO.

| Product Designer       | Period | Design Pair                 |
|------------------------|-----------|-----------------------------|
| Matt Nearents          | April-May | Gina Doyle                  |
